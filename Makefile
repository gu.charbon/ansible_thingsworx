.DEFAULT_GOAL := webstack

VERSION	:= 1.0
ROOT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
CUR_DATE := $(shell date +"%s")

PYTHON_INTERPRETER ?= python3.6
ACTIVATE := . venv/bin/activate

.PHONY: prepare-env

prepare-env:
	@$(PYTHON_INTERPRETER) -m virtualenv venv
	@$(ACTIVATE) && \
	pip install -r requirements.txt

update-env:
	@$(ACTIVATE) && \
	pip install -U -r requirements.txt

install:
	@$(ACTIVATE) && \
	ansible-playbook -i inventory.yml install_foundation.yml